import unittest

import urllib3
import json


class TestCase01(unittest.TestCase):

    def test1(self):
        """活动的流程实例"""
        http = urllib3.PoolManager()
        headers = urllib3.util.make_headers(basic_auth='admin:test')
        r = http.request('GET', 'http://admin:test@localhost:8080/flowable-task/process-api/runtime/process-instances', headers=headers)
        print(r.status)
        data = json.loads(r.data.decode('utf-8'))
        for item in data['data']:
            print(item)

    def test2(self):
        """任务列表"""
        http = urllib3.PoolManager()
        headers = urllib3.util.make_headers(basic_auth='admin:test')
        URL = 'http://admin:test@localhost:8080/flowable-task/process-api/runtime/tasks'
        r = http.request('GET', URL, headers=headers)
        print(r.status)
        data = json.loads(r.data.decode('utf-8'))
        for item in data['data']:
            print(item)

    def test3(self):
        """已部署的流程"""
        http = urllib3.PoolManager()
        headers = urllib3.util.make_headers(basic_auth='admin:test')
        URL = 'http://admin:test@localhost:8080/flowable-task/process-api/repository/deployments'
        r = http.request('GET', URL, headers=headers)
        print(r.status)
        data = json.loads(r.data.decode('utf-8'))
        for item in data['data']:
            print(item)


if __name__ == '__main__':
    unittest.TextTestRunner().run(unittest.TestSuite())
