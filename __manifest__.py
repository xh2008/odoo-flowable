# -*- coding: utf-8 -*-
{
    'name': "工作流管理",

    'summary': """
        工作流管理""",

    'description': """
        工作流管理
    """,

    'author': "西安亚软",
    'website': "xayaruan.com",

    'category': 'Uncategorized',
    'version': '1.0',

    'depends': ['base', ],
    'qweb': [
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
        'views/views_process.xml',
        'views/views_settings.xml',
    ],
    'demo': [
    ],
}
